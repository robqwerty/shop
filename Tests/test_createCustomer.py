from App.Models.Customer import Customer
from App.Models.Shop import Shop

class TestCustom():
    def test_customer(self):

        #arrange
        newCus = Customer("Bob", 123)

        #act
        #assert
        assert(newCus.naam == "Bob")
        assert(newCus.id == 123)


    def test_CreateCustomer(self):

        #arrange

        #act
        newCus = Shop.createCustomer()

        #assert
        assert(newCus.naam == "Bob")
        assert(newCus.id == 123)