from App.Models.Customer import Customer
from App.Models.Order import Order


class TestOrder():
    def test_order(self):
        #arrange
        newCus = Customer("Bob", 123)
        order = Order(23, newCus)
        #act

        #assert
        assert(order.product_Ids == 23)
        assert(order.Customer == newCus)
        assert(order.Customer.naam == "Bob")
        assert(order.Customer.id == 123)